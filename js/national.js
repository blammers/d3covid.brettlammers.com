import {
    numToPrettyString
} from './utility.js';

export function setNationalData(data) {
    let totalTested = data[0].positive + data[0].negative;
    let hospitalizedPercent = numToPrettyString(((data[0].hospitalizedCumulative/data[0].positive)*100).toFixed(2)) + "%";
    let deathPercent = numToPrettyString(((data[0].death/data[0].positive)*100).toFixed(2)) + "%";
    let positivePercent = numToPrettyString(((data[0].positive/totalTested)*100).toFixed(2)) + "%";
    let negativePercent = numToPrettyString(((data[0].negative/totalTested)*100).toFixed(2)) + "%";
    let recoveredPercent = numToPrettyString(((data[0].recovered/data[0].positive)*100).toFixed(2)) + "%";

    document.getElementsByName('national-tested')[0].innerText = numToPrettyString(totalTested);
    document.getElementsByName('national-positive')[0].innerText = numToPrettyString(data[0].positive);
    document.getElementsByName('national-hospitalized')[0].innerText = numToPrettyString(data[0].hospitalizedCumulative);
    document.getElementsByName('national-negative')[0].innerText = numToPrettyString(data[0].negative);
    document.getElementsByName('national-death')[0].innerText = numToPrettyString(data[0].death);
    document.getElementsByName('national-recovered')[0].innerText = numToPrettyString(data[0].recovered);
    document.getElementsByName('national-recovered-percent')[0].innerText = recoveredPercent;
    document.getElementsByName('national-date-checked')[0].innerText = data[0].dateChecked;
    document.getElementsByName('national-hospitalized-percent')[0].innerText = hospitalizedPercent;
    document.getElementsByName('national-death-percent')[0].innerText = deathPercent;
    document.getElementsByName('national-positive-percent')[0].innerText = positivePercent;
    document.getElementsByName('national-negative-percent')[0].innerText = negativePercent;
}

export function setNationalData2(data) {
    console.log(data)
    // let totalTested = data.confirmed;
    // let hospitalizedPercent = numToPrettyString(((data[0].hospitalized/confirmed)*100).toFixed(2)) + "%";
    // let deathPercent = numToPrettyString(((data[0].death/confirmed)*100).toFixed(2)) + "%";
    // let positivePercent = numToPrettyString(((confirmed/totalTested)*100).toFixed(2)) + "%";
    // let negativePercent = numToPrettyString(((data[0].negative/totalTested)*100).toFixed(2)) + "%";
    let recoveredPercent = numToPrettyString((data.calculated.recovery_rate).toFixed(2)) + "%";

    // document.getElementsByName('national-tested')[0].innerText = numToPrettyString(totalTested);
    document.getElementsByName('national-positive')[0].innerText = numToPrettyString(data.confirmed);
    // document.getElementsByName('national-hospitalized')[0].innerText = numToPrettyString(data[0].hospitalized);
    // document.getElementsByName('national-negative')[0].innerText = numToPrettyString(data[0].negative);
    document.getElementsByName('national-death')[0].innerText = numToPrettyString(data.death);
    document.getElementsByName('national-recovered')[0].innerText = numToPrettyString(data.recovered);
    document.getElementsByName('national-recovered-percent')[0].innerText = recoveredPercent;
    // document.getElementsByName('national-date-checked')[0].innerText = data[0].dateChecked;
    // document.getElementsByName('national-hospitalized-percent')[0].innerText = hospitalizedPercent;
    // document.getElementsByName('national-death-percent')[0].innerText = deathPercent;
    // document.getElementsByName('national-positive-percent')[0].innerText = positivePercent;
    // document.getElementsByName('national-negative-percent')[0].innerText = negativePercent;
}