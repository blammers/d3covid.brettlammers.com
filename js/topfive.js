import {
    numToPrettyString
} from './utility.js';

let TopFiveStates = {};

TopFiveStates.setTopFiveStates = (data) => {
    let nameSelect, code = "",
        fatality, fatality_percent, positive, positive_percent, hospitalized,
        hospitalized_percent, tested, recovered, recovered_percent;

    for (let index = 0; index < 5; index++) {
        tested = (data[index].totalTestResults) ? numToPrettyString(data[index].totalTestResults) : null;
        fatality_percent = ((data[index].death / data[index].positive) * 100).toFixed(2);
        fatality = (data[index].death) ? numToPrettyString(data[index].death) : null;
        positive = (data[index].positive) ? numToPrettyString(data[index].positive) : null;
        positive_percent = ((data[index].positive / data[index].totalTestResults) * 100).toFixed(2);
        hospitalized = (data[index].hospitalized) ? numToPrettyString(data[index].hospitalized) : null;
        hospitalized_percent = ((data[index].hospitalized / data[index].positive) * 100).toFixed(2);
        recovered = (data[index].recovered) ? numToPrettyString(data[index].recovered) : null;
        recovered_percent = ((data[index].recovered / data[index].positive) * 100).toFixed(2);

        nameSelect = "state-top-" + (index);

        code += "<div id=\"" + nameSelect + "\" name=\"" + data[index].state + "\" class=\"head-state-top-content\">" +
            "<div><strong>" + data[index].state + "</strong></div>" +
            "<div>Tested: " + tested + "</div>" +
            "<div>Positive: " + positive + " / " + positive_percent + "%</div>" +
            "<div>Hospitalized: " + hospitalized + " / " + hospitalized_percent + "%</div>" +
            "<div>Fatalities: " + fatality + " / " + fatality_percent + "%</div>" +
            "<div>Recovered: " + recovered + " / " + recovered_percent + "%</div>" +
            "</div>";
    }

    return code;
}

export default TopFiveStates;