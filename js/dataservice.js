export async function getStateData() {
    return fetch('./../data/states.json').then(response => {
        return response.json().then(data => {
            return data;
        })
    })
}

//https://covidtracking.com/api/v1/us/daily.json
export async function getNationalData() {
    return fetch('https://covidtracking.com/api/v1/us/current.json').then(response => {
        console.log(response)
        return response.json().then(data => {
            return data;
        })
    })
}

export async function getNationalData2() {
    return fetch('https://corona-api.com/countries/US').then(response => {
        return response.json().then(data => {
            return data;
        })
    })
}

export async function getCurrentCaseDataAllStates() {
    return fetch('https://covidtracking.com/api/states').then(response => {
        return response.json().then(data => {
            return data;
        })
    })
}

// export async function getCaseDataByState(state) {
//     let url='https://covidtracking.com/api/v1/states/daily.json?state=' + state;
//     console.log('async getCaseDataByState', state, url);
//     return fetch(url,{mode:'no-cors'}).then(response => {
//         return response.json().then(data => {
//             return data;
//         })
//     })
// }

export async function getCaseDataByState(state) {
    if (state === "test") {
        return d3.json('./testdata.json').then((results) => {
            return results;
        }).catch(err => {
            console.log(err)
        });
    } else if (state) {
        return d3.json('https://covidtracking.com/api/states/daily?state=' + state).then((results) => {
            return results;
        }).catch(err => {
            console.log(err)
        });
    } else {
        console.log(state)
        // chartData([]);
    }
}