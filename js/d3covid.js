import {
    numToPrettyString,
    trend
} from './utility.js';

import TopFiveStates from './topfive.js';

import {
    getStateData,
    getCurrentCaseDataAllStates,
    getCaseDataByState,
    getNationalData,
    // getNationalData2
} from './dataservice.js';

import {
    setNationalData,
    // setNationalData2
} from './national.js';

import {
    formatEnrichData
} from './daily.js';

import
d3chart
from './d3chart.js';

// settings
let testData = false;
// let key_height = 200;
// let margin = 20;
// let header_height = 240;
// let w = 800 - margin;
// let h = 600 - key_height - header_height;
// let ratio = w / h;
let resizeTimer;
let state = localStorage.getItem('location') ? localStorage.getItem('location') :"VA";
let cdata;
let loading = false;

let stateData;
let statesData;
let nationalData;
let selectedStateData;

init();

async function init() {
    initDisplayValues();

    // state meta data
    let fetchedStatesData = await getStateData();
    // process state data
    stateSelectionSetup(fetchedStatesData);
    statesData = fetchedStatesData;
    selectedStateData = getStateAttributesByAbbreviation(statesData, state);

    // case data by state - default VA
    let caseData = await getCaseDataByState(state);
    console.log(caseData)
    // enrich and process data
    let enrichedData = formatEnrichData(caseData, selectedStateData);
    // chartData(enrichedData);
    d3chart.chartData(enrichedData);
    cdata = enrichedData;

    // top 5 states
    let allStatesData = await getCurrentCaseDataAllStates();
    // calculate top states
    let enrichedAllStatesData = allStatesData.map(item => {
        item.totalScore = item.positive + item.hospitalized + item.death;
        return item;
    });

    enrichedAllStatesData.sort((a, b) => {
        return b.totalScore - a.totalScore;
    })

    let x = TopFiveStates.setTopFiveStates(enrichedAllStatesData);
    document.getElementById('state-top').innerHTML = x;
    document.getElementById('state-top-0').addEventListener("click", selectState);
    document.getElementById('state-top-1').addEventListener("click", selectState);
    document.getElementById('state-top-2').addEventListener("click", selectState);
    document.getElementById('state-top-3').addEventListener("click", selectState);
    document.getElementById('state-top-4').addEventListener("click", selectState);

    // national data
    let nationalData = await getNationalData();
    // console.log(nationalData)
    setNationalData(nationalData);
    console.log(nationalData)

    initChartDisplayToggles();
}

function initDisplayValues() {
    document.getElementsByName('national-positive')[0].innerText = "-";
    document.getElementsByName('national-hospitalized')[0].innerText = "-";
    document.getElementsByName('national-negative')[0].innerText = "-";
    document.getElementsByName('national-death')[0].innerText = "-";
    document.getElementsByName('national-tested')[0].innerText = "-";
    document.getElementsByName('national-recovered')[0].innerText = "-";

    document.getElementsByName('date')[0].innerText = "-";
    document.getElementsByName('tested')[0].innerText = "-";
    document.getElementsByName('positive')[0].innerText = "-";
    document.getElementsByName('hospitalized')[0].innerText = "-";
    document.getElementsByName('death')[0].innerText = "-";

    document.getElementsByName('delta-tested')[0].innerText = "-";
    document.getElementsByName('delta-positive')[0].innerText = "-";
    document.getElementsByName('delta-hospitalized')[0].innerText = "-";
    document.getElementsByName('delta-death')[0].innerText = "-";
}

function initChartDisplayToggles() {
    document.getElementById('chart-overall').addEventListener("change", toggleChartDisplay);
    document.getElementById('chart-delta').addEventListener("change", toggleChartDisplay);
    document.getElementById('chart-tested').addEventListener("change", toggleChartDisplay);
}

function toggleChartDisplay(e) {
    console.log('toggleChartDisplay', e.target.checked)
    switch (e.target.name) {
        case "chart-overall":
            d3chart.toggleChartShowOverall();
            d3chart.chartRedraw(cdata);
            break;
        case "chart-delta":
            d3chart.toggleChartShowDelta();
            d3chart.chartRedraw(cdata);
            break;
        case "chart-tested":
            d3chart.toggleChartShowTested();
            d3chart.chartRedraw(cdata);
            break;

        default:
            break;
    }
}

////////////////////////
function getStateAttributesByAbbreviation(stateData, abbreviation) {
    let results;

    stateData.forEach(item => {
        if (item.abbreviation == abbreviation) {
            results = item;
        };
    })
    // console.log('getStateAttributesByAbbreviation', stateData, abbreviation, results);
    return results;
}

function stateSelectionSetup(data) {
    // event listeners
    var sel = document.getElementById('select-state');
    sel.addEventListener("change", selectState);

    // setup selection options
    data.forEach(item => {
        var opt = document.createElement("option");
        if (item.abbreviation == state) {
            stateData = item;
            opt.setAttribute('selected', true);
        }
        opt.value = item.abbreviation;
        opt.text = item.name;
        sel.add(opt, null);
    });
}

async function selectState(e) {
    let stateValue;
    if (!e.target.value) {
        stateValue = e.target.childNodes[0].innerText;
    } else {
        stateValue = e.target.value;
    }

    localStorage.setItem("location", stateValue);

    if (stateValue) {
        var s = d3.selectAll('svg');
        s = s.remove();
        state = stateValue;
        selectedStateData = getStateAttributesByAbbreviation(statesData, state);

        let caseData = await getCaseDataByState(state);
        // // enrich and process data
        let enrichedData = formatEnrichData(caseData, selectedStateData);
        d3chart.chartData(enrichedData);
        cdata = enrichedData;
    }

    setMainTitle(e.target.text)
}

// window resize catch
window.onresize = function (event) {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function () {
        var s = d3.selectAll('svg');
        s = s.remove();
        d3chart.chartData(cdata);
    }, 100);
}

// key data
function populateData(data, reset) {
    document.getElementsByName('date')[0].innerText = reset ? "Roll over bar to display data for that date" : data.date;
    document.getElementsByName('tested')[0].innerText = reset ? 0 : data.total;
    document.getElementsByName('positive')[0].innerText = reset ? 0 : data.positive;
    document.getElementsByName('hospitalized')[0].innerText = reset ? 0 : data.hospitalized;
    document.getElementsByName('death')[0].innerText = reset ? 0 : data.death;
    document.getElementsByName('delta-tested')[0].innerText = reset ? 0 : data.delta_tested + "::" + data.trend.tested.description;
    document.getElementsByName('delta-positive')[0].innerText = reset ? 0 : data.delta_positive + "::" + data.trend.positive.description;
    document.getElementsByName('delta-hospitalized')[0].innerText = reset ? 0 : data.delta_hospitalized + "::" + data.trend.hospitalized.description;
    document.getElementsByName('delta-death')[0].innerText = reset ? 0 : data.delta_death + "::" + data.trend.death.description;
}

function setMainTitle(title) {
    document.getElementsByName('date')[0].innerText = "Roll over bar to display data for that date";
}