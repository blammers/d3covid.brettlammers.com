import {
    numToPrettyString,
    trend
} from './utility.js';

let trend_sample_size = 4;
let trend_percent = 10;

export function formatEnrichData(data, selectedStateData) {

    // console.log('formatEnrichData', selectedStateData)
    let cdata = {
        meta: {
            min: 0,
            max: 0,
            avg_fatality: 0,
            avg_hospitilized: 0
        },
        data: data.reverse()
    }

    for (let index = 0; index < cdata.data.length; index++) {

        let trend_sample = cdata.data.slice(index - trend_sample_size, index);
        cdata.data[index].trend = analyzeTrending(trend_sample);

        if (index > 0) {
            // console.log(cdata.data)
            cdata.data[index].delta_positive = Math.abs(cdata.data[index].positive - cdata.data[index - 1].positive);
            cdata.data[index].delta_negative = Math.abs(cdata.data[index].negative - cdata.data[index - 1].negative);
            cdata.data[index].delta_tested = Math.abs(cdata.data[index].delta_positive + cdata.data[index].delta_negative);
            cdata.data[index].delta_hospitalized = Math.abs(cdata.data[index].hospitalized - cdata.data[index - 1].hospitalized);
            cdata.data[index].delta_death = Math.abs(cdata.data[index].death - cdata.data[index - 1].death);
        } else {
            cdata.data[index].delta_positive = cdata.data[index].positive;
            cdata.data[index].delta_negative = cdata.data[index].negative;
            cdata.data[index].delta_hospitalized = cdata.data[index].hospitalized;
            cdata.data[index].delta_death = cdata.data[index].death;
        }

        cdata.data[index].death_percentage = (cdata.data[index].death / cdata.data[index].positive) * 100;
    }

    let x_trend_sample = cdata.data.slice(cdata.data.length - trend_sample_size, cdata.data.length);
    cdata.meta.trend = analyzeTrending(x_trend_sample);
    cdata.meta.latest_date = cdata.data[cdata.data.length - 1].dateChecked;
    cdata.meta.latest_positive = cdata.data[cdata.data.length - 1].positive;
    cdata.meta.latest_negative = cdata.data[cdata.data.length - 1].negative;
    cdata.meta.latest_death = cdata.data[cdata.data.length - 1].death;
    cdata.meta.latest_hospitalized = cdata.data[cdata.data.length - 1].hospitalized;
    cdata.meta.latest_hospitalized_currently = cdata.data[cdata.data.length - 1].hospitalizedCurrently ? cdata.data[cdata.data.length - 1].hospitalizedCurrently : cdata.meta.latest_hospitalized;
    cdata.meta.latest_hospitalized_released = cdata.meta.latest_hospitalized - cdata.meta.latest_hospitalized_currently - cdata.meta.latest_death;
    cdata.meta.latest_recovered = cdata.data[cdata.data.length - 1].recovered;

    cdata.meta.total_tested = (cdata.meta.latest_positive + cdata.meta.latest_negative);
    cdata.meta.selected_state = selectedStateData.name;
    cdata.meta.selected_state_population = selectedStateData.population;
    cdata.meta.percent_tested_population = ((cdata.meta.total_tested / cdata.meta.selected_state_population) * 100).toFixed(2);
    cdata.meta.percent_positive_population = ((cdata.meta.latest_positive / cdata.meta.selected_state_population) * 100).toFixed(2);

    cdata.meta.avg_hospitilized = ((cdata.data[cdata.data.length - 1].hospitalized / cdata.data[cdata.data
        .length - 1].positive) * 100);
    document.getElementsByName('avg-hospitalized')[0].innerText = cdata.meta.avg_hospitilized.toFixed(2) + "%";

    cdata.meta.avg_fatality = (cdata.data[cdata.data.length - 1].death / cdata.data[cdata.data.length - 1]
        .positive) * 100;
    document.getElementsByName('avg-death')[0].innerText = cdata.meta.avg_fatality.toFixed(2) + "%";

    return cdata;
}

function analyzeTrending(sampleArray) {
    // console.log(sampleArray)

    let results = {
        death: 0,
        hospitalized: 0,
        positive: 0,
        tested: 0
    }
    
    let trendArrayDeath = sampleArray.map(item => {
        return item.death;
    });
    let trendArrayHospitalized = sampleArray.map(item => {
        return item.hospitalized;
    });
    let trendArrayPositive = sampleArray.map(item => {
        return item.positive;
    });
    let trendArrayTested = sampleArray.map(item => {
        return item.totalTestResults;
    });

    if (trendArrayDeath.length !== 0) {
        results.death = trend(trendArrayDeath, trend_percent)
    }
    if (trendArrayHospitalized.length !== 0) {
        results.hospitalized = trend(trendArrayHospitalized, trend_percent)
    }
    if (trendArrayPositive.length !== 0) {
        results.positive = trend(trendArrayPositive, trend_percent)
    }
    if (trendArrayTested.length !== 0) {
        results.tested = trend(trendArrayTested, trend_percent)
    }

    // console.log(results);
    return results;
}
