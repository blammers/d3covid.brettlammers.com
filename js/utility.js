export function numToPrettyString(num) {
    if (num) {
        return num.toLocaleString();
    }

    return "";
}

export function trend(arr, thresholdPercent) {
    let results = {};
    let start, desc, direction, avg, trend, threshold;
    start = arr[0];
    let sum = arr.reduce(function (a, b) {
        return a + b
    }, 0);

    avg = sum / arr.length;
    trend = avg - start;
// console.log(thresholdPercent,(thresholdPercent * .01),Math.abs(avg))
    // calculate threshold percentage
    threshold = (thresholdPercent * .01) * Math.abs(start);
    let threshold_high = start + threshold;
    let threshold_low = start - threshold;
    // console.log("x",threshold, threshold_high,threshold_low)
    // trend
    // if ((trend < 0 && trend > (threshold * -1)) || (trend > 0 && trend < start + threshold)) {
    //     desc = "flat";
    //     direction = 0;
    // } 
    if (avg < threshold_low) {
        desc = "dec";
        direction = -1;
    } 
    else if (avg > threshold_high) {
        // console.log(trend,threshold)
        desc = "inc";
        direction = 1;
    } else {
        desc = "flat";
        direction = 0;
    }

    // console.log("y",threshold, desc)

    results = {
        "direction": direction,
        "description": desc,
        "trend": trend,
        "start": start,
        "average": avg,
        "threshold_amt": threshold,
        "threshold_low": threshold_low,
        "threshold_high": threshold_high
    }

    return results;
}