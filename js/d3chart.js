import {
    numToPrettyString
} from './utility.js';

let testData = false;
let key_height = 200;
let margin = 20;
let header_height = 240;
let w = 1000 - margin * 2;
let h = 600 - key_height - header_height;
let bar_width = 30;
let bar_width_delta = 30;
let bar_width_tested = 30;
let bar_margin = 5;
let ratio = w / h;
let resizeTimer;
let state = "VA";
// let cdata;
// let loading = false;

let chartShowOverall = true;
let chartShowDelta = true;
let chartShowTested = false;

let d3chart = {};

d3chart.chartData = function (cdata) {

    // console.log(w, cdata.data.length, w / cdata.data.length)

    d3chart.setWindowScale();
    let bar_width = (w / cdata.data.length) - (bar_margin * 2);
    let bar_width_delta = (w / cdata.data.length) - (bar_margin * 2);
    let bar_width_tested = (w / cdata.data.length) - (bar_margin * 2);

    // min max calc of nested data
    let vals = cdata.data.map(item => {
        let result = 0;
        if (chartShowOverall && !chartShowTested) {
            result = (item.positive ? item.positive : 0) +
                (item.hospitalized ? item.hospitalized : 0) +
                (item.death ? item.death : 0);
        } else if (!chartShowOverall && chartShowDelta && !chartShowTested) {
            result = (item.delta_positive ? item.delta_positive : 0) +
                (item.delta_hospitalized ? item.delta_hospitalized : 0) +
                (item.delta_death ? item.delta_death : 0);
        } else if (chartShowTested) {
            result = item.total;
        }
        return result;
    });
    // console.log(vals)
    cdata.meta.min = 0;
    cdata.meta.max = Math.max(...vals);
    let scale = d3.scaleLinear()
        .domain([cdata.meta.min, cdata.meta.max])
        .range([0, h - margin - 24]);

    // select and size chart
    let svg = d3.select("#chart").append("svg")
        .attr("width", w)
        .attr("height", h);

    // groups
    let chart = svg.selectAll("g")
        .data(cdata.data)
        .enter()
        .append("g")
        .attr("transform", function (d, i) {
            return ("translate(" + (w / cdata.data.length) * i + "," + -2 + ")");
        })
        .on('mouseover', function (d, i) {

            let bar = d3.select(this);
            let trend_data = bar.data()[0].trend;
            console.log(trend_data)


            let spaceBetweenBarsOnCenter = w / cdata.data.length;
            console.log(spaceBetweenBarsOnCenter)


            let trend_positive = bar.append("g").attr("class", "trend-positive");
            let trend_testing = bar.append("g");
            let trend_death = bar.append("g");
            let trend_hospitalized = bar.append("g");

            // let lineData = [];

            for (const key in trend_data) {

console.log(trend_data)
let trend_data_set = trend_data[key];


                trend_data[key].trendline = [
                    [
                        [spaceBetweenBarsOnCenter * (-3) - (spaceBetweenBarsOnCenter/2+bar_margin), h - scale(d.trend[key].start)],
                        [(spaceBetweenBarsOnCenter/2-bar_margin), h - scale(d.trend[key].average)]
                    ]
                ]

                trend_data[key].trendPoints = [
                    [spaceBetweenBarsOnCenter * (-3) - (spaceBetweenBarsOnCenter/2+bar_margin), h - scale(d.trend[key].start)],
                    [(spaceBetweenBarsOnCenter/2-bar_margin), h - scale(d.trend[key].average)]
                ]

                trend_data[key].thresholdPolygon = [
                    [
                        [(spaceBetweenBarsOnCenter/2-bar_margin), h - scale(d.trend[key].threshold_high)],
                        [spaceBetweenBarsOnCenter * (-3) - (spaceBetweenBarsOnCenter/2+bar_margin), h - scale(d.trend[key].start)],
                        [(spaceBetweenBarsOnCenter/2-bar_margin), h - scale(d.trend[key].threshold_low)]
                    ]
                ]
            }

            trend_positive.selectAll("circle").data(trend_data.positive.trendPoints)
                .enter().append("circle")
                .attr("cx", d => {
                    return d[0];
                })
                .attr("cy", d => {
                    return d[1];
                })
                .attr("r", 4)
                .attr("fill", "red")
                .attr("stroke-width", 2);


                trend_positive.selectAll("polygon").data(trend_data.positive.thresholdPolygon)
                .enter().append("polygon")
                .attr("points", d => {
                    console.log(d)
                    return d;
                })
                .attr("fill", "#29de1d33")
                .attr("stroke", "white")
                .attr("stroke-width", 1);


                trend_positive.selectAll("line").data(trend_data.positive.trendline).enter().append("line")
                .attr("x1", d => {
                    return d[0][0];
                })
                .attr("y1", d => {
                    return d[0][1];
                })
                .attr("x2", d => {
                    return d[1][0];
                })
                .attr("y2", d => {
                    return d[1][1];
                })
                .attr("stroke", "black")
                .attr("stroke-width", 2);

/////////////



//////////////

            // trend

            bar.transition()
                .duration('50')
                .attr('opacity', '.85');
            populateData(d);

        })
        .on('mouseout', function (d, i) {
            let bar = d3.select(this);
            bar.transition()
                .duration('50')
                .attr('opacity', '1');
            // populateData(d, true);
            // bar.selectAll("polygon").remove()
            bar.selectAll("g.trend-positive").transition()
            .duration('100').remove()

        })

        function test(d) {
            console.log(d)
        }

    // draw - plot data
    //tested
    if (chartShowTested) {
        let testedGroup = chart.append("g").attr("class","bar-tested");
        testedGroup.append("rect")
            .attr("width", bar_width_tested)
            .attr("height", (d) => {
                let height = d.total ? scale(d.total) : 0;
                return height;
            })
            .attr("transform", (d, i) => {
                return "translate(0," + (h - scale(d.total)) + ")"
            })
            .attr("class", "tested")

        testedGroup.append("text")
            .attr("x", bar_width_tested / 2)
            .attr("y", d => {
                return (h - scale(d.total) - 14)
            })
            .attr("dy", 0)
            .attr("text-anchor", "middle")
            .attr("dominant-baseline", "central")
            .text(function (d) {
                if (d.total > 0) return numToPrettyString(d.total);
            });
    }

    if (chartShowOverall) {
        let overallGroup = chart.append("g").attr("class","bar-overall");
        overallGroup.append("rect")
            .attr("width", bar_width)
            .attr("height", (d) => {
                let height = d.positive ? scale(d.positive) : 0;
                return height;
            })
            .attr("transform", (d, i) => {
                return "translate(0," + (h - scale(d.positive)) + ")"
            }).attr("class", "positive")

        overallGroup.append("rect")
            .attr("width", bar_width)
            .attr("height", (d) => {
                let height = d.hospitalized ? scale(d.hospitalized) : 0;
                return height;
            })
            .attr("transform", (d, i) => {
                return "translate(0," + (h - scale(d.positive)) + ")"
            })
            .attr("class", "hospitalized")

        overallGroup.append("rect")
            .attr("width", bar_width)
            .attr("height", (d) => {
                // console.log(d.death, scale(d.death))
                let height = d.death ? scale(d.death) : 0;
                return height;
            })
            .attr("transform", (d, i) => {
                return "translate(0," + (h - scale(d.positive)) + ")"
            })
            .attr("class", "death");

        if (!chartShowTested) {
            overallGroup.append("text")
                .attr("x", bar_width / 2)
                .attr("y", d => {
                    return (h - scale(d.positive) - 14)
                })
                .attr("dy", 0)
                .attr("text-anchor", "middle")
                .attr("dominant-baseline", "central")
                .text(function (d) {
                    if (d.positive > 0) return numToPrettyString(d.positive);
                });
        }
    }

    // delta
    if (chartShowDelta) {
        let deltaGroup = chart.append("g").attr("class","bar-delta");
        deltaGroup.append("rect")
            .attr("width", bar_width_delta)
            .attr("height", (d) => {
                let height = d.delta_positive ? scale(d.delta_positive) : 0;
                return height;
            })
            .attr("transform", (d, i) => {
                return "translate(0," + (h - scale(d.delta_positive)) + ")"
            })
            .attr("class", "delta-positive");

        deltaGroup.append("rect")
            .attr("width", bar_width_delta)
            .attr("height", (d) => {
                let height = d.delta_hospitalized ? scale(d.delta_hospitalized) : 0;
                return height;
            })
            .attr("transform", (d, i) => {
                return "translate(0," + (h - scale(d.delta_positive)) + ")"
            })
            .attr("class", "delta-hospitalized");

        deltaGroup.append("rect")
            .attr("width", bar_width_delta)
            .attr("height", (d) => {
                let height = d.delta_death ? scale(d.delta_death) : 0;
                return height;
            })
            .attr("transform", (d, i) => {
                return "translate(0," + (h - scale(d.delta_positive)) + ")"
            })
            .attr("class", "delta-death")

        if (!chartShowOverall && !chartShowTested) {
            deltaGroup.append("text")
                .attr("x", bar_width_delta / 2) //((w / cdata.data.length) - margin * 2) / 2)
                .attr("y", d => {
                    return (h - scale(d.delta_positive) - 14)
                })
                // .attr("dy", 0)
                .attr("text-anchor", "middle")
                .attr("dominant-baseline", "central")
                .text(function (d) {
                    if (d.delta_positive > 0) return numToPrettyString(d.delta_positive);
                });
        }
    }

    chart.exit().remove();

    let chartTextItems = [
        cdata.meta.latest_date,
        "Population: " + numToPrettyString(cdata.meta.selected_state_population),
        "Positive: " + numToPrettyString(cdata.meta.latest_positive),
        "Negative: " + numToPrettyString(cdata.meta.latest_negative),
        "Hospitalized (overall/current/released): " + numToPrettyString(cdata.meta.latest_hospitalized) + "/" +
        numToPrettyString(cdata.meta.latest_hospitalized_currently) + "/" +
        numToPrettyString(cdata.meta.latest_hospitalized_released),
        "Death: " + numToPrettyString(cdata.meta.latest_death),
        "Recovered: " + numToPrettyString(cdata.meta.latest_recovered),
        "% of state population tested: " + cdata.meta.percent_tested_population + "%",
        "% of state population positive: " + cdata.meta.percent_positive_population + "%"
    ]

    // state title
    svg
        .append("text")
        .attr("x", 20)
        .attr("y", 50)
        .attr("class", "svg-state-title")
        .text(cdata.meta.selected_state);

    for (let index = 0; index < chartTextItems.length; index++) {
        svg
            .append("text")
            .attr("x", 20)
            .attr("y", (index * 20) + 80)
            .text(chartTextItems[index]);
    }
}


function toggleChartDisplay(e) {
    // console.log('toggleChartDisplay', e.target.checked)
    switch (e.target.name) {
        case "chart-overall":
            chartShowOverall = !chartShowOverall
            chartRedraw();
            break;
        case "chart-delta":
            chartShowDelta = !chartShowDelta
            chartRedraw();
            break;
        case "chart-tested":
            chartShowTested = !chartShowTested
            chartRedraw();
            break;

        default:
            break;
    }
}

d3chart.chartRedraw = function (cdata) {
    console.log(cdata)
    var s = d3.selectAll('svg').remove();
    d3chart.setWindowScale();
    d3chart.chartData(cdata);
}

d3chart.setWindowScale = function () {
    let window_width = window.innerWidth;
    let window_height = window.innerHeight;
    let window_ratio = window_width / window_height;

    h = ((window_height - key_height - header_height) > 400) ? (window_height - key_height - header_height) : 400;
    w = window_width - margin - 10;
}

d3chart.toggleChartDisplay = function (e) {
    // console.log('toggleChartDisplay', e.target.checked)
    switch (e.target.name) {
        case "chart-overall":
            chartShowOverall = !chartShowOverall
            d3chart.chartRedraw(cdata);
            break;
        case "chart-delta":
            chartShowDelta = !chartShowDelta
            d3chart.chartRedraw(cdata);
            break;
        case "chart-tested":
            chartShowTested = !chartShowTested
            d3chart.chartRedraw(cdata);
            break;

        default:
            break;
    }
}

d3chart.toggleChartShowOverall = function () {
    chartShowOverall = !chartShowOverall
}

d3chart.toggleChartShowDelta = function () {
    chartShowDelta = !chartShowDelta
}

d3chart.toggleChartShowTested = function () {
    chartShowTested = !chartShowTested
}

function populateData(data, reset) {
    document.getElementsByName('date')[0].innerText = reset ? "Roll over bar to display data for that date" : data.date;
    document.getElementsByName('tested')[0].innerText = reset ? 0 : data.total;
    document.getElementsByName('positive')[0].innerText = reset ? 0 : data.positive;
    document.getElementsByName('hospitalized')[0].innerText = reset ? 0 : data.hospitalized;
    document.getElementsByName('death')[0].innerText = reset ? 0 : data.death;
    document.getElementsByName('delta-tested')[0].innerText = reset ? 0 : data.delta_tested + "::" + data.trend.tested.description;
    document.getElementsByName('delta-positive')[0].innerText = reset ? 0 : data.delta_positive + "::" + data.trend.positive.description;
    document.getElementsByName('delta-hospitalized')[0].innerText = reset ? 0 : data.delta_hospitalized + "::" + data.trend.hospitalized.description;
    document.getElementsByName('delta-death')[0].innerText = reset ? 0 : data.delta_death + "::" + data.trend.death.description;
}

export default d3chart;